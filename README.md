##Описание протокол для общения устройств IoT 
Протокол работает поверх MQTT 

 

### Со стороны устройства: 
- Устройство подключается к MQTT broker имея  
- IP/DNS адрес сервера 
- Порт подключения 
- Имя и пароль пользователя 

### На сервере для взаимодействия с устройством выделены две темы 
- «/devices/{device_id}/input» - эту тему устройство слушает, чтобы получать команды 
- «/devices/{device_id}/output» - в эту тему устройство шлёт данные 

Все данные передаются в формате JSON. При этом первые пакеты посылать в полной форме. То есть JSON пакет должен содержать все возможные поля, даже пустые. 

Формат пакетов зависит от устройства. 

Для логирлвания разработан скрипт на python. 

python make_config.py 
- Для начала просто через input 
- Этот скрипт создаёт конфигурацию для подключения к серверу в виде файла config_mqtt_server.json 

python mqtt_logging.py  
- Этот скрипт к папку (save-dir), создаёт папки с именами устройств, в каждой такой папке лежит два файла с input.csv и output.csv соответственно. 
- 

## На сервере:

```
sudo apt update -m
sudo apt install python3-pip
sudo apt update
pip3 install paho-mqtt

git clone https://gitlab.com/dodo325/iot-mqtt-python.git
cd iot-mqtt-python

lsblk
sudo parted /dev/sdb
mklabel msdos
mkpart primary ext4 1MiB 100%
quit
sudo mkfs.ext4 /dev/sdb1

sudo mount -t ext4 /dev/sdb1 /media
sudo chmod -R 777 /media

nohup python3 -u mqtt_logging.py </dev/null >/dev/null 2>&1 & 

```