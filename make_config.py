import json
with open('config_mqtt_server.json', 'w') as outfile:
    json.dump({'server': input("Enter server: "), 
                  'port': int(input("Enter port: ")),
		  'root_dir':input("Enter root_dir: "),
                  'user':input("Enter user: "),
                  'password':input("Enter password: ")
                }, 
               outfile,
               sort_keys=True, indent=4)
