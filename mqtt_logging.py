
import time
import paho.mqtt.client as mqtt

import csv
import json
import os.path

server ="m13.cloudmqtt.com"
port =12685
root_topic = 'devices/'
user= 'wplcvcuc'
password = 'Z2430JtHhK1v'
root_dir = ''
def load_config():
    with open('config_mqtt_server.json') as json_file:  
        data = json.load(json_file)
        global server, port, user,password, root_dir
        server = data['server']
        port = data['port']
        root_dir = data['root_dir']
        user = data['user']
        password = data['password']


def on_connect(client, userdata, rc):  
    print("Connected with result code: %s" % rc)

def file_write(path,columns):
    with open(path, 'a') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(columns)
    print('save:',columns, "\t|\tto ",path)

def on_message(client, userdata, msg):  
    timestamp = int(time.time())
    print("---")
    payload = msg.payload.decode("utf-8")

    path = root_dir +str(msg.topic)+ ".csv"
    directory = path[0:path.rfind('/')]
    if not os.path.exists(directory):
        os.makedirs(directory)
    try:
        data = json.loads(payload)
        data_list = [timestamp, msg.topic]

        if not os.path.isfile(path):
            colons =['timestamp', 'topic']

            for i in data.keys():
                colons.append(i)
            file_write(path, colons)

        for i in data.keys():
                data_list.append(data[i])

        file_write(path, data_list)
    except Exception:
        if not os.path.isfile(path):
            file_write(path, ['timestamp', 'topic','payload'])
        file_write(path, [timestamp, msg.topic, payload])


def main():  
    try:
        load_config()
    except Exception:
	    pass
    
    subscriber= mqtt.Client("client-server")
    subscriber.message_callback_add(root_topic+"#",on_message)

    print("connecting to broker ",server, 'port: ',port)
    subscriber.username_pw_set(user, password=password)
    subscriber.connect(server, port=port)#connect
    print("subscriber ",root_topic+"#")
  #  subscriber.publish(root_topic+"/client-server", "connected")
    subscriber.subscribe(root_topic+"#")
    subscriber.loop_forever()

if __name__ == "__main__":  
    main()
